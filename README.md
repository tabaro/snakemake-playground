# Containerization of Snakemake-based workflows

This repository is to illustrate Snakemake pipeline containerization. As a bonus, also the HTML report functionality is showcased.

1. Rules need to be coupled do Conda definitions
2. Snakemake can read such files and generate a Dockerfile
3. Gitlab CI can be used to automate building and store Docker images
4. Docker images can be transparently converted into Singularity image and Snakemake is able to automatically pull from remote repositories

## 1. Define Conda environment 

Using the `conda` directive it is possible to specify a Conda environment for a specific rule, therefore fixing dependencies and pinning specific versions of sofware packages.

```
rule some_rule:
    input:
        ...
    output:
        ...
    conda:
        "conda/deseq2.yml"
    params:
        ...
    script:
        ...
```

`conda/deseq.yaml`:

```yaml
channels:
  - bioconda
  - conda-forge
dependencies:
  - bioconductor-deseq2=1.34.0
```

Running `snakemake --use-conda` will result in generation of the defined conda environment in the local machine.

## 2. Automatic generation of Dockerfile

Use `snakemake --containerize` to generate a Dockerfile. Use redirections to write to a file named Dockerfile: 

```
snakemake --containerize > Dockerfile
```

## 3. Write a CI pipeline to automate Docker building.

At EMBL, we have a self-managed Gitlab instance. It also provides container registry. It is possible to use the CI feature to automate building of the Docker image defined previously and upload it directly to the container registry.

We use Kaniko to build image in a Docker container because CI jobs run inside Docker containers and building Docker in Docker is not allowed:

```yaml
.build-docker:
  stage: build
  image: 
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - mkdir -p /kaniko/.docker
    - echo "{\"auths\":{\"${CI_REGISTRY}\":{\"auth\":\"$(printf "%s:%s" "gitlab-ci-token" "${CI_JOB_TOKEN}" | base64 | tr -d '\n')\"}}}" > /kaniko/.docker/config.json
    - >-
      /kaniko/executor
      --context "${CI_PROJECT_DIR}"
      --dockerfile "${CI_PROJECT_DIR}/Dockerfile"
      --destination "${CI_REGISTRY}/${DOCKER_IMAGE}"
```

## 4. Define the container to run a pipeline in

By using the `containerized` directive, we instruct Snakemake to run the pipeline inside the container generated previously:

```
containerized: "docker://git.embl.de:4567/tabaro/snakemake-playground/env:fb47859b"
```

Now running `snakemake --use-conda --use-singularity` will trigger the download from the container registry, execution of the pipeline inside the Docker container as a Singularity image and activation of the Conda environments inside the container.

## 5. Builtin HTML report functionality

By wrapping output paths with the `report` function it is possible to tell Snakemake to collect such results into a HTML report. After a pipeline has successfully executed, run `snakemake --report report.html` to generate such file. An example report is attached to this repository. Report publishing can be automated using Gitlab pages functionality simplifing results sharing and allowing better organization of deliverables. 

