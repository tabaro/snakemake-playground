import os

configfile: "config.yaml"
container: "docker://continuumio/miniconda3:4.4.10"
containerized: "docker://git.embl.de:4567/tabaro/snakemake-playground/env:fb47859b"

rule all:
    input:
        expand(os.path.join(config["outdir"],"dataset-{i}/volcano-plot.png"), i=["a", "b", "c"])

rule mkdata:
    output:
        os.path.join(config["outdir"], "dataset-{i}/dds.rds")
    conda:
        "conda/deseq2.yml"
    params:
        ngenes=config["ngenes"],
        nsamples=config["nsamples"]
    script:
        "scripts/mkdataset.R"

rule runDESeq2:
    input:
        os.path.join(config["outdir"], "dataset-{i}/dds.rds")
    output:
        dds=os.path.join(config["outdir"], "dataset-{i}/dds-fitted-model.rds"),
        lfc_table=os.path.join(config["outdir"], "dataset-{i}/results-table.rds")
    conda:
        "conda/deseq2.yml"
    script:
        "scripts/run-deseq2.R"

rule plotVolcano:
    input:
        os.path.join(config["outdir"], "dataset-{i}/results-table.rds")
    output:
        report(os.path.join(config["outdir"], "dataset-{i}/volcano-plot.png"), 
               category="Volcano plot",
               subcategory="{i}")
    conda:
        "conda/tidyverse.yml"
    script:
        "scripts/plot-volcano.R"
