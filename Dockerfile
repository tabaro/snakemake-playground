FROM condaforge/mambaforge:latest
LABEL io.github.snakemake.containerized="true"
LABEL io.github.snakemake.conda_env_hash="17c5209019df5e5cb8e65369fe82140400e556e7ee63720ce70490fd3a18e4b2"

# Step 1: Retrieve conda environments

# Conda environment:
#   source: conda/deseq2.yml
#   prefix: /conda-envs/b056772eba85e9be2001fc43445c0205
#   channels:
#     - bioconda
#     - conda-forge
#   dependencies:
#     - bioconductor-deseq2=1.34.0
RUN mkdir -p /conda-envs/b056772eba85e9be2001fc43445c0205
COPY conda/deseq2.yml /conda-envs/b056772eba85e9be2001fc43445c0205/environment.yaml

# Conda environment:
#   source: conda/tidyverse.yml
#   prefix: /conda-envs/b341a1fb1380dc0a23ec278200bab694
#   channels:
#     - bioconda
#     - conda-forge
#   dependencies:
#     - r-tidyverse=1.3.2
#     - bioconductor-deseq2=1.34.0
RUN mkdir -p /conda-envs/b341a1fb1380dc0a23ec278200bab694
COPY conda/tidyverse.yml /conda-envs/b341a1fb1380dc0a23ec278200bab694/environment.yaml

# Step 2: Generate conda environments

RUN mamba env create --prefix /conda-envs/b056772eba85e9be2001fc43445c0205 --file /conda-envs/b056772eba85e9be2001fc43445c0205/environment.yaml && \
    mamba env create --prefix /conda-envs/b341a1fb1380dc0a23ec278200bab694 --file /conda-envs/b341a1fb1380dc0a23ec278200bab694/environment.yaml && \
    mamba clean --all -y
